import os
from setuptools import setup

with open('requirements.txt', 'r') as f:
    requirements = f.read().splitlines()

version = '-1'
try:
    version_file_name = os.path.join('VERSION')
    version_fs = open(version_file_name)
    version = version_fs.read().strip()
    version_fs.close()
except Exception:
    print("Warning: Version can not be accurately identified")

print("Installing xprctl version: {}".format(version))
# Setup configuration
setup(
    name='xprctl',
    version=version,
    packages=['xpresso'],
    entry_points={
        'console_scripts': [
            'xprctl = xpresso.ai.client.xprctl:cli_options'
        ]
    },
    description="Python command line application bare bones template.",
    author="Naveen Sinha",
    author_email="naveen.sinha@abzooba.com",
    install_requires=requirements
)

